/*
 * lcd.h
 *
 *  Created on: 27 févr. 2014
 *  Modified on: 19.05.2015
 *  Author: F. Vannel, V. Pilloux
 */

#ifndef LCD_H_
#define LCD_H_

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#define INTER_CHAR_SPACING 4
#define NB_CHAR_MIN (128/(8+INTER_CHAR_SPACING)+1)
#define CHAR_WIDTH (8+INTER_CHAR_SPACING)

/* LCD screen initialisation */
void init_lcd(void);

/* Allocates memory for a bitmap of 'string_size_max'. The function
* returns the number of horizontal characters which could be "written" to the bitmap or
* -1 if an error occurred. Note that the minimum size returned and allocated (in case
* of allocation success) is NB_CHAR_MIN.
* The bitmap effective size is 32 x string_size_max*CHAR_WIDTH bits, where the dimensions
* are height x width. The bitmap width can be larger than the maximum size of the screen (128):
* this allows the user to specify an horizontal offset telling what portion of the bitmap
* must be displayed (see string_to_bitmap()).
* Parameter: string_size_max: maximum number of characters the bitmap contains
* Return: number of characters allocated or -1 if an error occurred
*/
int init_bitmap(int string_size_max);

/* Forms a bitmap of 32 x C*CHAR_WIDTH bits based on a string, where C=string length
* The bitmap has a height of 32 bits (1 word) and the word index represents
* the horizontal offset of the word. Even if it is possible to "write" a string at any horizontal
* offset of the bitmap, the screen being only 128 pixels wide, only a portion of the
* whole bitmap can be displayed.
* Note that init_bitmap() must be called before using this function.
* Parameters:
* x_bit_offset: original horizontal offset of the string
* y_bit_offset:  0: write only first "line" of 16 bits
*                16: write only second "line" of 16 bits (starting from 16th pixel)
*                other: write only one line on full screen at y_bit_offset position
* Return: 0 in case of success, -1 if the string length is too large, if bad offset or
*         if init_bitmap() has not been called before this function
*/
int string_to_bitmap(char *str, unsigned x_bit_offset, unsigned y_bit_offset);

/* Displays the bitmap starting from an arbitrary horizontal offset.
 * The horizontal offset must not by higher than string_size_max*CHAR_WIDTH-128.
 * Parameter: x_bit_offset: horizontal offset of the bitmap representing the
 *            starting position of the image to display
* Return: 0 in case of success, -1 if bad offset or if init_bitmap() has not been
*         called before this function
 */
int display_bitmap(int x_bit_offset);

/* Displays a matrix of 32 x 128 pixels directly on the screen (height x width).
 * The lower bit of each 32 bits word is placed at the top of the screen.
 * Parameter: pic: 32 bits x 128 word vector representing the pixel matrix
 */
void display_picture(unsigned int pic[]);


#endif /* LCD_H_ */
