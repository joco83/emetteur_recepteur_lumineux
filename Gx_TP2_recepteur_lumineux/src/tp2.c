/**
 * Name        : main.c
 * Version     :
 * Description : main definition for FreeRTOS application
 */

#include <cr_section_macros.h>
#include <NXP/crp.h>
#include "LPC17xx.h"
#include <stdio.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "read_rgb.h"
#include "lcd.h"
#include "debug.h"

#define BUF_LEN 5000		// taille du tampon d'acquisition ou de traitement
#define STR_LEN_MAX 13		// taille maximum de la chaîne de caractère reçue sans la séquence de référence
#define SAMPLES_PER_BIT 20	// 20 échantillons par bit sont reçus
#define FILT_LEN 10			// longueur du filtre à moyenne glissante
#define SEQ_BIT_LEN (2*8)   // taille de la séquence de référence en bits

int32_t corr_buf[BUF_LEN/2];  // tampon pour résultat de la corrélation
int16_t ref_seq[SEQ_BIT_LEN*SAMPLES_PER_BIT]; // tampon pour les échantillons de la séquence de référence
char *seq_ref_str="\x95\x1b";				  // séquence de référence
unsigned int image[128];					  // "image" du bitmap du LCD

//  bss_RAM2 (RamAHB32) utilisée pour éviter dépassement de capacité mémoire RamLoc32
__DATA(RAM2) uint16_t rec_buf[2][BUF_LEN];		  // double tampon pour la réception des échantillons du capteur RGB
__DATA(RAM2) int16_t filt_buf[BUF_LEN-FILT_LEN+1];  // tampon de stockage du signal filtré
int buffer_index;							  // index sur tampon de traitement (provenant du capteur RGB)




int main(void)
{
	// lancement de l'acquisition des échantillons:
	//read_rgb_init(rec_buf, your_end_function);

	// initialisation de l'écran LCD
	init_lcd();
	if (init_bitmap(2*STR_LEN_MAX)<0)
		EXIT("Error in init_bitmap()!");
	// formation du contenu de l'image à afficher
	if (string_to_bitmap("PTR=bol d'R", 0, 11)<0)
		EXIT("Error in string_to_bitmap()!");
	// affichage de l'image
	display_bitmap(0);
	while(1);
	return 1;
}
