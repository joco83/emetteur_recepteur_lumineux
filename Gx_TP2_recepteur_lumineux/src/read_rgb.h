/*
 * read_led.h
 *
 * Created on: 3 mai 2015
 * Author: V. Pilloux
 */

#ifndef READ_LED_H_
#define READ_LED_H_

#include <stdint.h>

#define BUFFER_LENGTH 5000

/* Prototype of callback function called when one of both reception buffers
 * are filled.
 * Parameter: buffer_index: index of the buffer just filled
 */
typedef void (*func_t)(int buffer_index) ;

/* Initialize and start the sampling of the RGB sensor. The sampling rate
 * is 5 kHz. First buffer 0 is filled with the sum of the values coming from
 * each color of the RGB sensor. At completion, the callback function
 * "callback_func" is called to let the user know that a buffer is now "usable".
 * Then the function automatically fills the second buffer, then go back to first
 * buffer and so on.
 * Note: the function uses timer 3.
 *
 * Parameters: buffer: pointer on a double buffer for the samples receiving
 *             callback_func: callback function call when one of both buffer
 *                            is filled.
 */
void read_rgb_init(uint16_t buffer[2][BUFFER_LENGTH], func_t callback_func);

/* Stops the RGB sensor samples receiving */
void read_rgb_stop();

#endif /* READ_LED_H_ */
