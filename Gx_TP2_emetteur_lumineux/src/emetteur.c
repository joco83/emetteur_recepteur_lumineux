/*
 * emetteur.c
 *
 * Date: 19.05.2015
 * Auteur: V. Pilloux
 */
#include <cr_section_macros.h>
#include <NXP/crp.h>
#include <stdio.h>
#include <string.h>

#include "LPC17xx.h"
#include "ledrgb_dma.h"


#define WHITE 0xFFFFFF
#define BLACK 0


int main (void)
{
	// séquence de référence à envoyer au début de chaque chaîne de caractère
	char *seq="\x95\x1B";
	volatile int i;
	int color=WHITE;

	// exemple d'utilisation de la led
	while(1)
	{
		set_rgb_led_color(color);
		for(i=0; i<5000000; i++);
		color^=WHITE;
	}
}


