/*
 * ledrgb_dma.h
 *
 *  Created on: Mar 26, 2014
 *      Author: Adrien, V. Pilloux
 */

#ifndef LEDRGB_DMA_H_
#define LEDRGB_DMA_H_

#include "LPC17xx.h"

/**
 * fill ws2812 24bit  color array
 * rgb has to be 7:0 blue
 *               15:8 green
 *               23:16 red
 *               31:24 reserved
 */
void set_rgb_led_color(uint32_t rgb);


#endif /* LEDRGB_DMA_H_ */
