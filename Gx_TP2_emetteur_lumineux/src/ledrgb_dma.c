#include "LPC17xx.h"

static uint8_t ws2812_color[96];


/**
 * Turn on RGB LED with the specified color
 * ws2812 color has a size of 96 and has to be filled with rgb_to_ws2812()
 * Every single bit of the 24 is divided in 4. So the timings are a multiple of 300ns
 * T0H = 300ns
 * T0L = 900ns
 * T1H = 600ns
 * T1L = 600ns
 * Timer3 is used at 100Mhz (10ns)
 * Every 300ns one byte is sent
 */
void set_RGB_led()
{
	LPC_GPIO0->FIODIR |= 1<<21;

	//Timer 3
	LPC_SC->PCONP |= 1<<23; // Power up timer 3
	LPC_SC->PCLKSEL1 |= 1 << 14; // CCLK on timer 3
	LPC_TIM3->MR0 = 33; //300ns + ... (originally 30)
	LPC_TIM3->MCR = 1 << 1; //reset on Match Compare 0

	//DMA
	LPC_SC->PCONP |= 1 << 29; //power up
	LPC_GPDMA->DMACConfig |= 1<<0; //enable GPDMA

	LPC_SC->DMAREQSEL |= 1<<6; // Timer3 Match Compare 0 as DMA

	//Clear the Interrupt Terminal Count Request and Interrupt Error Status register.
	LPC_GPDMA->DMACIntErrClr |= 0xff;
	LPC_GPDMA->DMACIntTCClear |= 0xff;

	//Set the source and destination addresses
	LPC_GPDMACH0->DMACCDestAddr = (uint32_t) &(LPC_GPIO0->FIOPIN2); // RGB led is on P0.21
	LPC_GPDMACH0->DMACCSrcAddr = (uint32_t) &ws2812_color[0];

	LPC_GPDMACH0->DMACCControl = 96 | ( 1<<26 ); //transfert size = 96 bytes, increment source after transfert

	//set memory to perif and terminal count interrupt
	LPC_GPDMACH0->DMACCConfig = (14<<6) | (1<<11);

	LPC_GPDMACH0->DMACCConfig |= 1; //enable DMA channel 0

	LPC_TIM3->IR |= 0xff; // Clear all timer interrupts if there are any
	LPC_TIM3->TCR = 0x01; // start timer.
}

/**
 * fill ws2812 24bit  color array
 * rgb has to be 7:0 blue
 *               15:8 green
 *               23:16 red
 *               31:24 reserved
 * ws2812_color has a size of 96
 * Only bit 5 (or GPIO->FIOPIN2 or P0.21) is taken into account
 */
void set_rgb_led_color(uint32_t rgb)
{
	uint8_t i, j=0;
	for(i=8; i<16; i++) // red
	{
		ws2812_color[j++] = 1<<5;
		ws2812_color[j++] = ((rgb>>i)&1) ? (1<<5) : 0;
		ws2812_color[j++] = 0;
		ws2812_color[j++] = 0;
	}
	for(i=16; i<24; i++) // green
	{
		ws2812_color[j++] = 1<<5;
		ws2812_color[j++] = ((rgb>>i)&1) ? (1<<5) : 0;
		ws2812_color[j++] = 0;
		ws2812_color[j++] = 0;
	}
	for(i=0; i<8; i++) // blue
	{
		ws2812_color[j++] = 1<<5;
		ws2812_color[j++] = ((rgb>>i)&1) ? (1<<5) : 0;
		ws2812_color[j++] = 0;
		ws2812_color[j++] = 0;
	}
	set_RGB_led();
}
